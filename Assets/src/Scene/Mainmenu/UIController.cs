﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    [SerializeField] private UnityEngine.UI.Text controllerText;

    void Start () {
        if (PlayerPrefs.GetInt("controller")==0)
        {
            PlayerPrefs.SetInt("controller", 1);
            Debug.Log("FirstRun");
        }
        StreamReader ds = new StreamReader("version.txt");
        string TextFile = ds.ReadToEnd();
        controllerText.text = TextFile;
    }
	
    public void OnSettingsCkick()
    {
        SceneManager.LoadScene("Settings");
    }
    public void OnStartClick()
    {
       
        SceneManager.LoadScene("FirstScene");
        Debug.Log("Succefull Load");
    }

    public void OnAlphaClick()
    {

        SceneManager.LoadScene("Loading");
        Debug.Log("Succefull Load");
    }


    public void OnExit()
    {
        Application.Quit();
    }
}
