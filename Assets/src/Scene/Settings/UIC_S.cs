﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIC_S : MonoBehaviour {


    [SerializeField] private UnityEngine.UI.Button controllerText;

    void Start () {
        if (PlayerPrefs.GetInt("controller") == 1)
        {
            controllerText.GetComponentInChildren<Text>().text = "Controller off";


        }
        else if (PlayerPrefs.GetInt("controller") == 2)
        {
            controllerText.GetComponentInChildren<Text>().text = "Controller on";
        }
        else
        {
            controllerText.GetComponentInChildren<Text>().text = "Error";
        }
        
	}
	
    public void OnControllerButtonClick()
    {
        if (PlayerPrefs.GetInt("controller") == 1)
        {
            controllerText.GetComponentInChildren<Text>().text = "Controller on";

            PlayerPrefs.SetInt("controller", 2);

        }
        else
        {
            controllerText.GetComponentInChildren<Text>().text = "Controller off";
            PlayerPrefs.SetInt("controller", 1);
        }
        Debug.Log(PlayerPrefs.GetInt("controller"));
    }

    public void OnBackToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
