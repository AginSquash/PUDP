﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class SceneController : MonoBehaviour
    {

        [SerializeField] private Light D_light;
        public bool DebugStatus;
        public int TimeAll = 24;

        void Start()
        {
            StartCoroutine(DayNightTime(true, DebugStatus));

            Debug.Log("Start Scene");

        }


        void Update()
        {

        }


        private IEnumerator DayNightTime(bool Start, bool DebugStatus)
        {
            if (!DebugStatus)
            {
                float DeltaY = 100f;
                while (Start == true)
                {
                    if (DeltaY > -140)
                    {
                        D_light.transform.rotation = Quaternion.Euler(0f, DeltaY, 0f);
                        DeltaY -= 1f;
                    }
                    else
                    {
                        DeltaY = 100f;
                    }
                    yield return new WaitForSeconds(6f);
                }
            }
            else
            {

            }

        }

    }