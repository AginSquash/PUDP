﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIC_FS : MonoBehaviour {

    [SerializeField] private SettingsPopup settingsPopup;
     void Start()
    {
        settingsPopup.Close();
    }

    public void OnOpenSettings()
    {
        settingsPopup.Open();
    }

    public void OnCloseSettings()
    {
        settingsPopup.Close();
    }
    public void OnMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        Debug.Log("Succefull Load");
    }
    
}
