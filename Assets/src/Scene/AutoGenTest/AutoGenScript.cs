﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AutoGenScript : MonoBehaviour {

    [SerializeField] public bool DebugStatus;

    [SerializeField] private GameObject enemyPrefab;
    private GameObject _enemy;


    [SerializeField] private GameObject texturePrefab;
    private GameObject[] texture = new GameObject[999999];

    [SerializeField] private CharacterController _charController;



    public float minDistace = 30f;

    private int maxEnemy=40;
    // Use this for initialization
    void Start () {


        if (!DebugStatus)
        {
             OnStartGen();
             CheckEnemy(); 
        }
    }
	
	// Update is called once per frame
	void Update () {

       // CheckEnemy();

    }

    public void CheckEnemy()
    {
        if (_enemy == null)
        {
            for (int i = 0; i <= maxEnemy; i++)
            {
                _enemy = Instantiate(enemyPrefab) as GameObject;
                _enemy.transform.position = new Vector3(UnityEngine.Random.Range(-20, 50), UnityEngine.Random.Range(-20, 50), 0f);
                float angle = UnityEngine.Random.Range(0, 360);
                _enemy.transform.Rotate(0, 0, angle);
            }
            maxEnemy *= 2;
        }
    }

    private void OnStartGen()
    {
        float x = -30, y = -30;
        for (int i=0; i<=300; i++)
        {
            for (int g = 0; g <= 300; g++)
            {
                texture[i] = Instantiate(texturePrefab) as GameObject;
                texture[i].transform.position = new Vector3(x, y, 2);
                x += 5;
            }
            y += 5;
            x = -30;
        }
    }

    private void Test()
    {
        float x = -50, y = -50;
        for (int g = 0; g <= 1000; g++)
        {
            for (int i = 0; i <= 1000; i++)
            {
              //  texture = Instantiate(texturePrefab) as GameObject;
              //  texture.transform.position = new Vector3(x, y, 2);
                x += 5;
            }
            y += 5;
            x = -50;
        }
    }

  /*  private void Distance_check()
    {
        Vector3 point_FW = new Vector3(_cameraFw.pixelWidth / 2, _cameraFw.pixelHeight / 2, 0);
        Ray ray_FW = _cameraFw.ScreenPointToRay(point_FW);
        RaycastHit hit_FW1;
       
        if (Physics.Raycast(ray_FW, out hit_FW1))
        {
            if (hit_FW1.distance <= 20f)
            {
                //  WorldGen();
                

                FW_end.transform.position = new Vector3(FW_end.transform.position.x, FW_end.transform.position.y + 30, FW_end.transform.position.z);
                BH_end.transform.position = new Vector3(BH_end.transform.position.x, BH_end.transform.position.y + 30, BH_end.transform.position.z);
                LF_end.transform.position = new Vector3(LF_end.transform.position.x, LF_end.transform.position.y + 30, LF_end.transform.position.z);
                RG_end.transform.position = new Vector3(RG_end.transform.position.x, RG_end.transform.position.y + 30, RG_end.transform.position.z);

              //  Gen();
                
                Debug.Log("1 error");
                
            }
            Debug.Log("2 error");
            hit_FW = hit_FW1;
        }

        Vector3 point_BH = new Vector3(_cameraBh.pixelWidth / 2, _cameraBh.pixelHeight / 2, 0);
        Ray ray_BH = _cameraBh.ScreenPointToRay(point_BH);


        if (Physics.Raycast(ray_BH, out hit_BH))
        {
            if (hit_BH.distance <= 20f)
            {
                FW_end.transform.position = new Vector3(FW_end.transform.position.x, FW_end.transform.position.y - 30, FW_end.transform.position.z);
                BH_end.transform.position = new Vector3(BH_end.transform.position.x, BH_end.transform.position.y - 30, BH_end.transform.position.z);
                LF_end.transform.position = new Vector3(LF_end.transform.position.x, LF_end.transform.position.y - 30, LF_end.transform.position.z);
                RG_end.transform.position = new Vector3(RG_end.transform.position.x, RG_end.transform.position.y - 30, RG_end.transform.position.z);

               // Gen();
            }
        }

        Vector3 point_LF = new Vector3(_cameraLf.pixelWidth / 2, _cameraLf.pixelHeight / 2, 0);
        Ray ray_LF = _cameraLf.ScreenPointToRay(point_LF);


        if (Physics.Raycast(ray_LF, out hit_LF))
        {
            if (hit_LF.distance <= 20f)
            {
                FW_end.transform.position = new Vector3(FW_end.transform.position.x - 30, FW_end.transform.position.y, FW_end.transform.position.z);
                BH_end.transform.position = new Vector3(BH_end.transform.position.x - 30, BH_end.transform.position.y, BH_end.transform.position.z);
                LF_end.transform.position = new Vector3(LF_end.transform.position.x - 30, LF_end.transform.position.y, LF_end.transform.position.z);
                RG_end.transform.position = new Vector3(RG_end.transform.position.x - 30, RG_end.transform.position.y, RG_end.transform.position.z);

              //  Gen();
            }
        }

        Vector3 point_RG = new Vector3(_cameraRg.pixelWidth / 2, _cameraRg.pixelHeight / 2, 0);
        Ray ray_RG = _cameraRg.ScreenPointToRay(point_RG);


        if (Physics.Raycast(ray_RG, out hit_RG))
        {
            if (hit_RG.distance <= 20f)
            {
                FW_end.transform.position = new Vector3(FW_end.transform.position.x + 30, FW_end.transform.position.y, FW_end.transform.position.z);
                BH_end.transform.position = new Vector3(BH_end.transform.position.x + 30, BH_end.transform.position.y, BH_end.transform.position.z);
                LF_end.transform.position = new Vector3(LF_end.transform.position.x + 30, LF_end.transform.position.y, LF_end.transform.position.z);
                RG_end.transform.position = new Vector3(RG_end.transform.position.x + 30, RG_end.transform.position.y, RG_end.transform.position.z);

              //  Gen();
            }
        }

        Debug.Log(hit_FW.distance + " " + hit_BH.distance + " " + hit_LF.distance + " " + hit_RG.distance);
    }

    */
    public void Saver()
    {
        StreamReader ds = new StreamReader("test.txt");
        string TextFile = ds.ReadToEnd();
        Char delimiter = '/';
        String[] substrings = TextFile.Split(delimiter);
        foreach (var substring in substrings)
            Debug.Log(substring);

    }

  /*( private void WorldGen()
    {
       // Double db;
       // Double.TryParse(hit_FW.distance.ToString(), out db);
     //   int i = int.Parse((Math.Round(db, 0)).ToString());
        
        while ((i % 5)!=0){
            if (i < 0)
            {
                i--;
            }
            else
            {
                i++;
            }
        }
        
        for (int g = 0; g <= 60; g++)
        {
            texture[1] = Instantiate(texturePrefab) as GameObject;
            texture[1].transform.position = new Vector3(0, i, 2);
            i += 5;
            Debug.Log("Gen " + i);
        }
    } 

    private void Gen()
    {
        Vector3 pos = _charController.transform.position;

        float x = pos.x, x1 = pos.x;
        float y = pos.y;

        for (int i = 0; i <= 6; i++)
        {
            for (int g = 0; g <= 12; g++)
            {
                texture[i] = Instantiate(texturePrefab) as GameObject;
                texture[i].transform.position = new Vector3(x, y, 2);
                x += 5;
            }
            y += 5;
            x = x1;
        }
    }
    */
   /*( private void CleanMemory()
    {
        for (int i =0; i<100; i++)
        {
            if (texture[i].transform.position.y <= BH_end.transform.position.x)
            {
                Destroy(texture[i]);
            }
        }
    } */
}
