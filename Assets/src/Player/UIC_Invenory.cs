﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIC_Invenory : MonoBehaviour {

    [SerializeField] private InventoryVisible inventoryVisible;
    void Start()
    {
        inventoryVisible.OnClose();
    }

    public void OnOpenInventory()
    {
        inventoryVisible.OnOpen();
    }

    public void OnCloseInventory()
    {
        inventoryVisible.OnClose();
    }
}
