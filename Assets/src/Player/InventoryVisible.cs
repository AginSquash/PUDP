﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryVisible : MonoBehaviour
{

    [SerializeField] private UnityEngine.UI.Text InventoryListTest;

    public void OnOpen()
    {
        gameObject.SetActive(true);
        GetInvTest();
    }

    public void OnClose()
    {
        gameObject.SetActive(false);
    }

    private void GetInvTest()
    {
        List<string> itemList = Managers.Inventory.GetItemList();
        if (itemList.Count == 0)
        {
            InventoryListTest.text = "Empety";
        }

            InventoryListTest.text = "";
            foreach (string item in itemList)
            {
                int count = Managers.Inventory.GetItemCount(item);
                InventoryListTest.text +=item +": " + count + "\n";
            }
  
    }
}
