﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class RayShooter : MonoBehaviour {

     [SerializeField] private GameObject bulletPrefab;
     private GameObject _bulletVisble;
     [SerializeField] public FPSInput Finput;

   // [SerializeField] private AudioClip shootSound;
    [SerializeField] private AudioSource shootSound0;
    [SerializeField] private AudioSource shootSound1;
    [SerializeField] private AudioSource shootSound2;
    [SerializeField] private AudioSource shootSound3;
    [SerializeField] private AudioSource shootSound4;
    [SerializeField] private AudioSource shootSound5;
    [SerializeField] private AudioSource shootSound6;
    [SerializeField] private AudioSource shootSound7;
    [SerializeField] private AudioSource shootSound8;


    public float obstacleRange = 18.0f;
    public AudioClip pistolShoot;
    private int WeaponNum = 0;
    public const int AllWeaponNum = 5;
    public float radius = 20f;


  //  private Camera _camera;
    private bool controller = false;
    private bool shoot0_playing = false;
    private bool shoot1_playing = false;
    private bool shoot2_playing = false;
    private bool shoot3_playing = false;
    private bool shoot4_playing = false;
    private bool shoot5_playing = false;
    private bool shoot6_playing = false;
    private bool shoot7_playing = false;
    private bool shoot8_playing = false;



    // Use this for initialization
    void Start () {
     //   _camera = GetComponent<Camera>();
        if (PlayerPrefs.GetInt("controller") == 1)
        {
            controller = false;
        }
        else if (PlayerPrefs.GetInt("controller") == 2)
        {
            controller = true;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (!controller)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Shoot();
            }
        }
        else 
        {
            if (Input.GetAxis("PS4_R2") < 0.05f)
            {
                Shoot();
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (WeaponNum < (AllWeaponNum))
            {
                WeaponNum++;
            }
            else
            {
                WeaponNum = 0;
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (WeaponNum > 0)
            {
                WeaponNum--;
            }
            else
            {
                WeaponNum = AllWeaponNum;
            }

        }
       // Debug.Log(WeaponNum);
    }

    public void OnCam(bool up)
    {
        if (up)
        {
            obstacleRange = 36.0f;
        } else
        {
            obstacleRange = 18.0f;
        }
    }

    public void Shoot()
    {
        if (Finput.GetAccesToShoot())
        {
            ShootSettings(pistolShoot);

            Collider[] hitColliders =
                        Physics.OverlapSphere(transform.position, radius);
            foreach (Collider hitCollider in hitColliders)
            {
                hitCollider.SendMessage("Detected", SendMessageOptions.DontRequireReceiver);
            }


            _bulletVisble = Instantiate(bulletPrefab) as GameObject;
            _bulletVisble.transform.position = transform.TransformPoint(Vector3.forward * 0.02f + new Vector3(0f, 0f, 0.3f)); //z = 0.3f
            _bulletVisble.transform.rotation = transform.rotation;

            /* Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);
             Ray ray = _camera.ScreenPointToRay(point);
             RaycastHit hit;

             if (Physics.Raycast(ray, out hit))
             {

                   _bulletVisble = Instantiate(bulletPrefab) as GameObject;
                 _bulletVisble.transform.position = transform.TransformPoint(Vector3.forward * 0.01f);
                    _bulletVisble.transform.rotation = transform.rotation;
                 Debug.Log(hit.point);

                 if (hit.distance <= obstacleRange)
                 {
                     if (hit.collider.tag == "enemy")
                     {
                         Destroy(hit.collider.gameObject); //Уничтожить объект, в который бросил луч (необязательно, это моя проверка).
                         Debug.Log("Perfect shoot!");
                     }
                 }
                 else
                 {
                     Debug.Log("Too long");
                 }

             } */
        }
    }
    

    public void ShootSettings(AudioClip sound)
    {
        shootSound0.clip = sound;
        shootSound1.clip = sound;
        shootSound2.clip = sound;
        shootSound3.clip = sound;
        shootSound4.clip = sound;
        shootSound5.clip = sound;
        shootSound6.clip = sound;
        shootSound7.clip = sound;
        shootSound8.clip = sound;

        if (!shoot0_playing)
        {
            StartCoroutine(Shot0SoundPlay());
            shoot0_playing = true;
        }
        else if (!shoot1_playing)
        {
            StartCoroutine(Shot1SoundPlay());
            shoot1_playing = true;
        }
        else if (!shoot2_playing)
        {
            StartCoroutine(Shot2SoundPlay());
            shoot2_playing = true;
        }
        else if (!shoot3_playing)
        {
            StartCoroutine(Shot3SoundPlay());
            shoot3_playing = true;
        }
        else if (!shoot4_playing)
        {
            StartCoroutine(Shot4SoundPlay());
            shoot4_playing = true;
        }
        else if (!shoot5_playing)
        {
            StartCoroutine(Shot5SoundPlay());
            shoot5_playing = true;
        }
        else if (!shoot6_playing)
        {
            StartCoroutine(Shot6SoundPlay());
            shoot6_playing = true;
        }
        else if (!shoot7_playing)
        {
            StartCoroutine(Shot7SoundPlay());
            shoot7_playing = true;
        }
        else if (!shoot8_playing)
        {
            StartCoroutine(Shot8SoundPlay());
            shoot8_playing = true;
        }
        else
        {
            Debug.Log("Too many shoots");
        }

    }


    private IEnumerator Shot0SoundPlay()
    {
        shootSound0.Play();
        yield return new WaitForSeconds(shootSound0.clip.length);
        shoot0_playing = false;
    }
    private IEnumerator Shot1SoundPlay()
    {
        shootSound1.Play();
        yield return new WaitForSeconds(shootSound1.clip.length);
        shoot1_playing = false;
    }
    private IEnumerator Shot2SoundPlay()
    {
        shootSound2.Play();
        yield return new WaitForSeconds(shootSound2.clip.length);
        shoot2_playing = false;
    }
    private IEnumerator Shot3SoundPlay()
    {
        shootSound3.Play();
        yield return new WaitForSeconds(shootSound3.clip.length);
        shoot3_playing = false;
    }
    private IEnumerator Shot4SoundPlay()
    {
        shootSound4.Play();
        yield return new WaitForSeconds(shootSound4.clip.length);
        shoot4_playing = false;
    }
    private IEnumerator Shot5SoundPlay()
    {
        shootSound5.Play();
        yield return new WaitForSeconds(shootSound5.clip.length);
        shoot5_playing = false;
    }
    private IEnumerator Shot6SoundPlay()
    {
        shootSound6.Play();
        yield return new WaitForSeconds(shootSound6.clip.length);
        shoot6_playing = false;
    }
    private IEnumerator Shot7SoundPlay()
    {
        shootSound7.Play();
        yield return new WaitForSeconds(shootSound7.clip.length);
        shoot7_playing = false;
    }
    private IEnumerator Shot8SoundPlay()
    {
        shootSound8.Play();
        yield return new WaitForSeconds(shootSound8.clip.length);
        shoot8_playing = false;
    }
}
