﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceOperator : MonoBehaviour {

    public float radius = 1.5f;
    [SerializeField] private Camera _camera;

	void Start () {
		
	}
	
	
	void Update () {
        if (Input.GetButtonDown("Device"))
        {

            Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);
            Ray ray = _camera.ScreenPointToRay(point);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.distance <= radius)
                {
                    if (hit.collider != null)
                    {
                        hit.collider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);
                    }
                 }
                else
                {
                    
                }


                /*
                Debug.Log("E pressed");
                Collider[] hitColliders =
                    Physics.OverlapSphere(transform.position, radius);
                foreach (Collider hitCollider in hitColliders)
                {
                    Ray ray = new Ray(transform.position, transform.forward);
                    RaycastHit hit;
                    if (Physics.SphereCast(ray, 0.75f, out hit))
                    {
                        if (hit.distance <= radius)
                        {
                            Debug.Log("Success!");
                            hitCollider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);
                        }
                    } 
                   Vector3 direction = hitCollider.transform.position - transform.position;
                    if (Vector3.Dot(transform.forward , direction) > .5f)
                    {
                        Debug.Log("Success!");
                        hitCollider.SendMessage("Operate", SendMessageOptions.DontRequireReceiver);
                    }  */
            }
        }
	}
}
