﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {


    public float sensHor = 9.0f;
    public float sensVer = 9.0f;
    public float  maxSpeed = 4.0f;
    private bool controller = true;
    // Use this for initialization
    void Start () {
        if (PlayerPrefs.GetInt("controller") == 1)
        {
            controller = false;
        }
        else if (PlayerPrefs.GetInt("controller") == 2)
        {
            controller = true;
        }
    }

    private Vector3 mousePosition;
    public float moveSpeed = 2f;

    void Update()
    {
       
        if (controller)
            {
                float deltaX = Input.GetAxis("PS4_RH");
                float deltaY = Input.GetAxis("PS4_RV");
                if (Mathf.Abs(deltaX) > 0.03f || Mathf.Abs(deltaY) > 0.03f)
                {
                    float rotation_z1 = Mathf.Atan2(deltaY, deltaX) * Mathf.Rad2Deg;
                    transform.rotation = Quaternion.Euler(0f, 0f, rotation_z1);
                }
                Debug.Log("Controller on!");

            }
            else if (!controller)
            {
                mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                transform.position = Vector2.MoveTowards(transform.position, mousePosition, 0);

                Vector3 difference = mousePosition - transform.position;
                difference.Normalize();
                float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0f, 0f, rotation_z);
            }
        }
    
}
