﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FPSInput : MonoBehaviour {

    [SerializeField] private UIC_Invenory inventoryVisible;
    [SerializeField] private UIC_FS settingsPopup;
    [SerializeField] private RayShooter rayShooter;
    [SerializeField] private Light lantern;
    private CharacterController _charController;

   

    public float speed = 3f;
    float rezSpeed;
    bool zoom = false, zoomOff = false;
    private bool controller = true;
    public bool isInvOpen = false;
    public bool isMenuOpen = false;


    void Start () {

        _charController = GetComponent<CharacterController>();
        if (PlayerPrefs.GetInt("controller") == 1)
        {
            controller = false;
        }
        else if (PlayerPrefs.GetInt("controller") == 2)
        {
            controller = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
 
        if (Input.GetKeyDown(KeyCode.Escape)) //in-game Menu
        {
            if (!isMenuOpen)
            {

                settingsPopup.OnOpenSettings();
                isMenuOpen = true;
            }
            else
            {
                settingsPopup.OnCloseSettings();
                isMenuOpen = false;
            }
        }
        if (!isMenuOpen)
        {
            if (Input.GetKeyDown(KeyCode.I)) //Inventory
            {
                Debug.Log("I Pressed");
                if (!isInvOpen)
                {

                    inventoryVisible.OnOpenInventory();
                    isInvOpen = true;
                }
                else
                {
                    inventoryVisible.OnCloseInventory();
                    isInvOpen = false;
                }

            }
            if (!isInvOpen)
            {

                if (Input.GetKeyDown(KeyCode.LeftShift))
                {
                    speed = speed * 2;
                }

                if (Input.GetKeyUp(KeyCode.LeftShift))
                {
                    speed = speed / 2;
                }
                float deltaX = Input.GetAxis("Horizontal") * speed;
                float deltaY = Input.GetAxis("Vertical") * speed;

                Vector2 movement = new Vector2(deltaX * Time.deltaTime, deltaY * Time.deltaTime);
                _charController.Move(movement);

                if (!controller)
                {
                    if ((Input.GetMouseButtonDown(1) == true))
                    {

                        rezSpeed = speed;
                        StartCoroutine(CamSize(true));
                        rayShooter.OnCam(true);
                        speed = 3f;

                    }
                    if ((Input.GetMouseButtonUp(1) == true))
                    {
                        StartCoroutine(CamSize(false));
                        rayShooter.OnCam(false);
                        speed = rezSpeed;

                    }

                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        if (lantern.intensity == 0)
                        {
                            lantern.intensity = 1;
                        }
                        else
                        {
                            lantern.intensity = 0;
                        }
                    }
                }
                else if (controller)
                {
                    if (Input.GetAxis("PS4_L2") < 0.05f)
                    {
                        if (!zoom)
                        {
                            rezSpeed = speed;
                            StartCoroutine(CamSize(true));
                            rayShooter.OnCam(true);
                            speed = 3f;
                            zoom = true;
                            zoomOff = true;
                        }
                    }

                    if (Input.GetAxis("PS4_L2") > 0.05f)
                    {
                        if (zoomOff)
                        {
                            StartCoroutine(CamSize(false));
                            rayShooter.OnCam(false);
                            speed = rezSpeed;
                            zoomOff = false;
                            zoom = false;
                        }

                    }
                }
            }
        }
    }

    private IEnumerator CamSize(bool up)
    {
         
             for (float i = 7; i < 50; i++)
             {
                if (up)
                 {
                     Camera.main.orthographicSize += 0.1f;
            }
                 else
                 {
                    Camera.main.orthographicSize -= 0.1f;
                 }
                yield return new WaitForSeconds(0.01f);
             }
       
    }

    public bool GetAccesToShoot()
    {
        if (isMenuOpen||isInvOpen)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
