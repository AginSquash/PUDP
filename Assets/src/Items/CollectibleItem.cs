﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleItem : MonoBehaviour {

    [SerializeField] public string itemName;

    private void OnTriggerEnter(Collider other)
    {
        GameObject obj = other.gameObject;
        if (obj.tag == "player")
        {
            Debug.Log("Item collected: " + itemName);
            Managers.Inventory.AddItem(itemName);
            Destroy(gameObject);
        }
    }
}
