﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Text;

public class Save : MonoBehaviour {

    public GameObject Player;
    public CharacterController _charController;

    public void OnSave()
    {
        XmlTextWriter invWriter = new XmlTextWriter(Application.persistentDataPath + "/invetory.xml", System.Text.Encoding.UTF8);
        invWriter.WriteStartDocument();
        invWriter.WriteStartElement("head");
        invWriter.WriteEndElement();
        invWriter.Close();
        XmlDocument invDoc = new XmlDocument();
        invDoc.Load(Application.persistentDataPath + "/invetory.xml");
        XmlNode INVelement = invDoc.CreateElement("INVENTORY");
        invDoc.DocumentElement.AppendChild(INVelement); // указываем родителя
        XmlAttribute INVattribute = invDoc.CreateAttribute("name"); // создаём атрибут
        INVattribute.Value = "AGINS_ALPHA_TESTER"; // устанавливаем значение атрибута

        List<string> itemList = Managers.Inventory.GetItemList();
        XmlNode[] itmesX = new XmlNode[10];
        int i = 0;
        foreach (string item in itemList)
        {
            itmesX[i] = invDoc.CreateElement(item);
            //INVelement.Attributes.Append(INVattribute); // добавляем атрибут
            itmesX[i].InnerText = item + "/" + Managers.Inventory.GetItemCount(item).ToString(); // и значение
            INVelement.AppendChild(itmesX[i]);

          //  XmlNode key = invDoc.CreateElement("Keyr"); // даём имя
          //  key.InnerText = ""; // и значение
           // INVelement.AppendChild(key); // и указываем кому принадлежит
         //   int count = Managers.Inventory.GetItemCount(item);
            i++;
        }
        invDoc.Save(Application.persistentDataPath + "/invetory.xml");
        Debug.Log(Application.persistentDataPath + "/invetory.xml  COMPLETE!");
        


        XmlTextWriter textWritter = new XmlTextWriter(Application.persistentDataPath+"/save.xml", System.Text.Encoding.UTF8);
         textWritter.WriteStartDocument();
         textWritter.WriteStartElement("head");
         textWritter.WriteEndElement();
         textWritter.Close(); 
         XmlDocument document = new XmlDocument();
         document.Load(Application.persistentDataPath + "/save.xml");
        XmlNode element = document.CreateElement("PlayerSettings");
        document.DocumentElement.AppendChild(element); // указываем родителя
        XmlAttribute attribute = document.CreateAttribute("name"); // создаём атрибут
        attribute.Value = "AGINS_ALPHA_TESTER"; // устанавливаем значение атрибута

        XmlNode coordinate = document.CreateElement("coordinate");
        element.Attributes.Append(attribute); // добавляем атрибут

        XmlNode x = document.CreateElement("X-cor"); // даём имя
        x.InnerText = Player.transform.position.x.ToString(); // и значение
        element.AppendChild(x); // и указываем кому принадлежит

        XmlNode y = document.CreateElement("Y-cor");
        y.InnerText = Player.transform.position.y.ToString();
        element.AppendChild(y);

        XmlNode z = document.CreateElement("Z-cor");
        z.InnerText = Player.transform.position.z.ToString();
        element.AppendChild(z);

        XmlNode health = document.CreateElement("health");
        health.InnerText = "100";
        element.AppendChild(health);

        document.Save(Application.persistentDataPath + "/save.xml");
        Debug.Log(Application.persistentDataPath + "/save.xml");

     //  // XmlNode itmes = invDoc.CreateElement("items");
      //  INVelement.Attributes.Append(INVattribute); // добавляем атрибут

       // XmlNode key = invDoc.CreateElement("Keyr"); // даём имя
       // key.InnerText =  // и значение
       // INVelement.AppendChild(key); // и указываем кому принадлежит

     //   XmlNode keyCount = invDoc.CreateElement("KeyCount");
      //  y.InnerText = Player.transform.position.y.ToString();
      //  INVelement.AppendChild(y);



        
    }

    public void OnLoad()
    {
        
        string[] loaded_data = new string[10];
        XmlDocument doc = new XmlDocument();
        doc.Load(Application.persistentDataPath + "/save.xml");
        XmlNode child = doc.SelectSingleNode("head/PlayerSettings");
        if (child != null)
        {
            XmlNodeReader nr = new XmlNodeReader(child);
            int i = 0;
            int num = 0;
            while (nr.Read())
                if (i == 2)
                {
                    loaded_data[num] = nr.Value;
                    Debug.Log(loaded_data[num]);
                    num++;
                    i = 0;
                }
                else
                {
                    i++;
                }
        }

        _charController.Move( new Vector3 (float.Parse(loaded_data[0]), float.Parse(loaded_data[1]), float.Parse(loaded_data[2])));


        string[] loaded_inv = new string[100];
        XmlDocument inv = new XmlDocument();
        inv.Load(Application.persistentDataPath + "/invetory.xml");
        XmlNode child_inv = inv.SelectSingleNode("head/INVENTORY");
        if (child_inv != null)
        {
            XmlNodeReader nr = new XmlNodeReader(child_inv);
            int i = 0;
            int num = 0;
            while (nr.Read())
            {
                if (i == 2)
                {
                      loaded_inv[num] = nr.Value;
                      string[] items = loaded_inv[num].Split(new char[] { '/' }, System.StringSplitOptions.RemoveEmptyEntries);
                      for (int g=0; g!=int.Parse(items[1]); g++ )
                      {
                        Managers.Inventory.AddItem(items[0]);
                      }
                      Debug.Log("Inv: "+loaded_inv[num]);
                      num++;
                      i = 0;
                }
                else
                {
                    i++;
                }
            }
        }
        


    }
}

