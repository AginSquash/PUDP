﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpenDevice : MonoBehaviour {

    [SerializeField] private Vector3 dpos;
    [SerializeField] private Vector3 rotatG;
    private bool _open = false;
    private Quaternion basePos;

	public void Operate()
    {
        if (_open)
        {

          //  Vector3 pos = transform.position - dpos;
           // transform.position = pos;
            transform.rotation = Quaternion.Euler(basePos.x, basePos.y, basePos.z);
        }
        else
        {
            basePos = transform.rotation;
            transform.rotation = Quaternion.Euler(rotatG);
        }
        _open = !_open;
    }
}
