﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class bullet : MonoBehaviour {

    public float speed = 30.0f;
    // Use this for initialization
    void Start () {
        Destroy(gameObject, 5f);
    }
	
	// Update is called once per frame
	void Update () {

        transform.Translate(0, 0, speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject obj = other.gameObject;
        if (obj.tag != "player")
        {
            if (obj.tag == "enemy")
            {
                BaseAI enemy = other.GetComponent<BaseAI>();
                enemy.HurtEnemy(1);
                Debug.Log("Enemy hit");
            }
            Destroy(gameObject);
        }
    }

}
