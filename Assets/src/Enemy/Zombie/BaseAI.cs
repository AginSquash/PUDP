﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAI : MonoBehaviour {

        public float speed = 1f;
        public float obstacleRange = 10f;
        public float radius = 10f;
        public int _health = 2;

        private GameObject player;
        private bool _alive;
        private float TimeToFinish;
        private bool isPlanEnd = true;
        private bool player_detected = false;


        public void Detected()
        {
            player_detected = true;
            radius = 20f;
            StartCoroutine(RadiusChanger());
        }
        // Use this for initialization
        void Start() {
            _alive = true;
            player = GameObject.FindGameObjectWithTag("player");

    }

    // Update is called once per frame
    void Update() {
         //   Debug.Log("Radius: " + radius);
            if (_alive)
            {
                if (!isPlanEnd)
                {
                    if (TimeToFinish <= 0)
                    {
                        Ray ray = new Ray(transform.position, transform.forward);
                        RaycastHit hit;
                        if (Physics.SphereCast(ray, 0.75f, out hit))
                        {
                            if (hit.collider.tag != "player")
                            {
                                if (hit.distance < obstacleRange)
                                {
                                    float deltaYR = Random.Range(-110, 110);
                                    float deltaXR = Random.Range(-110, 110);
                                    float rotation_z1 = Mathf.Atan2(deltaYR, deltaXR) * Mathf.Rad2Deg;
                                    transform.rotation = Quaternion.Euler(0f, 0f, rotation_z1);
                                }
                            }
                            else
                            {
                                player_detected = true;
                            }
                        }
                        var distance = (player.transform.position - transform.position).magnitude;
                        if (distance < radius)
                        {
                            player_detected = true;
                        }
                        if (player_detected)
                        {
                            if (distance < radius)
                            {
                            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, 0);
                            Vector3 difference = player.transform.position - transform.position;
                            difference.Normalize();
                            float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
                            transform.rotation = Quaternion.Euler(0f, 0f, rotation_z);
                            transform.position = Vector3.Lerp(transform.position, player.transform.position, speed * Time.deltaTime);
                        }
                            else
                            {
                                player_detected = false;
                            }
                        }
                        else
                        {

                            float deltaX = Random.Range(0.1f, 1f) * speed;
                            float deltaY = Random.Range(0.1f, 1f) * speed;

                            Vector2 movement = new Vector2(deltaX * Time.deltaTime, deltaY * Time.deltaTime);
                           transform.Translate(movement);
                        }
                    }
                    else
                    {
                        TimeToFinish -= 1 * Time.deltaTime;
                        if (TimeToFinish <= 0)
                        {
                            isPlanEnd = true;
                        }
                        Debug.Log(TimeToFinish);
                    }
                    //Code for AI
                }
                else
                {
                    GenreatePlane();
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            GameObject obj = other.gameObject;
            if (obj.tag == "player")
            {
                PlayerCharacter playerCharacter = other.GetComponent<PlayerCharacter>();
            try
            {
                playerCharacter.Hurt(1);
            }
            catch
            {
                //Т.к. игрок состоит из нескольких объектов, дамаг проходит не по всем, а тоько по однуму, в угоду баланса
            }
            }
            else
            {
                float deltaYR = Random.Range(-110, 110);
                float deltaXR = Random.Range(-110, 110);
                float rotation_z1 = Mathf.Atan2(deltaYR, deltaXR) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.Euler(0f, 0f, rotation_z1);
            }

        }

        void GenreatePlane()
        {
            System.Random num = new System.Random();
            Debug.Log(num.Next(0, 2));
            switch (num.Next(0, 1))
            {
                case 0: StopAI(); break;
                case 1: break;
            }
        }
        void StopAI()
        {
            TimeToFinish = 0;//Random.Range(10, 180f);
            isPlanEnd = false;
        }

        private IEnumerator RadiusChanger()
        {
            yield return new WaitForSeconds(15f);
            radius = 10f;
        }

        public void HurtEnemy(int damage)
        {
             _health -= damage;
            if (_health <= 0)
        {
            
            Destroy(gameObject);
        } 
        }
}